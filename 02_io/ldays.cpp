
#include <stdio.h> 
#include <stdlib.h>

/* Función punto de entrada */
int main(){

    int d, m, a;
    int dias_vividos, date_d, date_m, date_a;


    printf("Fecha de nacimiento: \n");
    scanf("%i/%d/%i", &d, &m, &a);
    printf("¿Fecha de hoy?: \n");
    scanf("%i/%d/%i", &date_d, &date_m, &date_a);
    dias_vividos = (date_a - a)*365 + (date_m - m)*30 + (date_d - d);
    printf("has vivido %i dias\n", dias_vividos);


    return EXIT_SUCCESS;
}
