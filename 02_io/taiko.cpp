
#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h>

/* Función punto de entrada */
int main(){  

    // printf tiene escritura bufferizada	
    fprintf(stderr,"\a");//stderr no esta bufferizado
    usleep(100000);
    fputc('\a', stderr);
    usleep(1000000);
    printf("\a\n");
   
   

    return EXIT_SUCCESS;
}
