

#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h>

#define VECES 30
#define BTU 100000 /*Basic Time Unit */

/* Función punto de entrada */
int main(){  
   int duracion[VECES] = { 2, 0, 8, 2, 0, 8, 2, 0, 8, 2, 2, 1, 1, 1, 1, 1, 1, 8, 2, 0, 2, 0, 8 };
	//bucle for: Debe valer para repetir cosas 	
	for(int i=0; i<VECES; i++) {	
		usleep(duracion[i] * BTU);
		fputc('\a', stderr);
	}



	return EXIT_SUCCESS;
}
