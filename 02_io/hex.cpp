
#include <stdio.h> 
#include <stdlib.h>

/* Función punto de entrada */
int main(){
    int numero;

    printf("inserte un numero: ");
    
    scanf("%i", &numero);
    printf(" 0x%X\n", numero); // %x para cambiar a hexadecimal, %X para que las letras esten en mayusculas

    return EXIT_SUCCESS;
}
